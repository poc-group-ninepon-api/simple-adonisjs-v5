import { schema, CustomMessages, rules } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class UserValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    username: schema.string({ escape: true, trim: true },[
      rules.minLength(6),
      rules.maxLength(12),
      rules.regex(/^[a-zA-Z0-9-_]+$/),
      rules.unique({ table: 'users', column: 'username' }),
      rules.notIn(['admin', 'super', 'moderator', 'public', 'dev', 'alpha', 'mail']),
      // rules.alphaNum({
      //     allow: ['underscore', 'dash']
      // })
    ]),
    email: schema.string({ trim: true },[
        rules.unique({ table: 'users', column: 'email' }),
        rules.email({
          ignoreMaxLength: true,
          allowIpDomain: true,
          domainSpecificValidation: true,              
        })
    ]),
    password: schema.string(),
    fullname: schema.string(),
    tel: schema.string([
        rules.mobile({
          locales: ['pt-BR', 'en-IN', 'en-US', 'th-TH']
        })
    ]),
  });

  public messages = {}
}
