import { schema, CustomMessages, rules } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class UserValidator {
  constructor(protected ctx: HttpContextContract) {}
  
  public schema = schema.create({
    username: schema.string({ escape: true, trim: true },[
      rules.minLength(6),
      rules.maxLength(12),
    ]),
    email: schema.string({ trim: true },[
      rules.email({
        ignoreMaxLength: true,
        allowIpDomain: true,
        domainSpecificValidation: true,              
      })
    ]),
    fullname: schema.string(),
    tel: schema.string([
      rules.mobile({
        locales: ['pt-BR', 'en-IN', 'en-US', 'th-TH']
      })
    ]),
  });

  public messages = {}
}
