import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'
import Hash from '@ioc:Adonis/Core/Hash'
import { string } from '@ioc:Adonis/Core/Helpers'

import StoreUserValidator from 'App/Validators/Users/StoreValidator'
import UpdateUserValidator from 'App/Validators/Users/UpdateValidator'

export default class UsersController {
    public async index({ auth, request, response }: HttpContextContract) {
        return response.status(200).json()
    }

    public async store({ auth, request, response }: HttpContextContract) {
        let payload = await request.validate(StoreUserValidator)
        try{
            const findDuplicateUsers = await User.query().where('username', payload.username).first()
            if(findDuplicateUsers){
                return response.status(409).json({message: `duplicate username user`})
            }
            payload.user_code = string.generateRandom(16)
            payload.password = await Hash.make(payload.password)
        
            await User.create(payload)
            return response.status(203).json()
        }catch(err){
            return response.status(503).json({message: `error: ${err}`})
        }
    }

    public async destroy({ auth, request, response }: HttpContextContract) {
        let payload = await request.validate({ schema: schema.create({ username: schema.string() }) })
        const getUser = User.query()
        try{
            const findDuplicateUsers = await getUser.where('username', payload.username).first()
            if(findDuplicateUsers){
                await getUser.where(payload).delete()
                return response.status(203).json()
            }else{
                return response.status(404).json({message: `can't found this user via this username`})
            }
        }catch(err){
            return response.status(503).json({message: `error: ${err}`})
        }
    }

    public async show({ auth, request, response, params }: HttpContextContract) {
        try{
            let getUser = await User.query().where('id', params.id).first()
            if(getUser){
                getUser = getUser.toJSON()
                delete getUser.password
                delete getUser.id
                return response.status(200).json(getUser)
            }
        }catch(err){
            return response.status(503).json({message: `error: ${err}`})
        }
    }

    public async update({ auth, request, response }: HttpContextContract) {
        let payload = await request.validate(UpdateUserValidator)
        const getUser = User.query().where('username', payload.username)
        try{
            const findDuplicateUsers = await getUser.first()
            if(findDuplicateUsers){
                await getUser.update(payload)
                return response.status(203).json()
            }else{
                return response.status(404).json({message: `can't found this user via this username`})
            }
        }catch(err){
            return response.status(503).json({message: `error: ${err}`})
        }
    }

    public async refreshToken({ auth, request, response }: HttpContextContract){
        const refreshToken = request.input("refresh_token");
        try{
            const jwt = await auth.use("jwt").loginViaRefreshToken(refreshToken);
            return response.status(200).json({
                token: token.accessToken
                refreshToken: token.refreshToken
                expire: token.expiresAt
            })
        }catch(err){
            return response.status(503).json({message: `error: ${err}`})
        }
    }

    public async login({ auth, request, response }: HttpContextContract) {
        const username = request.input('username')
        const entriePassword = request.input('password')
        try{
            const user = await User
                .query()
                .where('username', username)
                .firstOrFail()

            if (!(await Hash.verify(user.password, entriePassword))) {
                return response.unauthorized('Invalid credentials')
            }
            
            const token = await auth.use('jwt').generate(user)
            return response.status(200).json({
                token: token.accessToken
                refreshToken: token.refreshToken
                expire: token.expiresAt
            })
        }catch(err){
            return response.status(503).json({message: `error: ${err}`})
        }
    }

    public async logout({ auth, request, response }: HttpContextContract) {
        try{
            await auth.use('jwt').revoke()
            return {
                revoked: true
            }
        }catch(err){
            return response.status(503).json({message: `error: ${err}`})
        }
    }

    public async registerAdmin({ auth, request, response }: HttpContextContract) {
        const username = request.input('username')
        const password = request.input('password')
          
        const hashedPassword = await Hash.make(password)
        console.log(hashedPassword)
    }
}
