import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Drive from '@ioc:Adonis/Core/Drive'
import { extname } from 'path'

export default class PersonalsController {
    
    public async uploadImagePersonToS3({ auth, request, response }: HttpContextContract) {
        let filenameWithType = null
        let typeName = null
        let body = {};

        request.multipart.onFile('image', {
            extnames: ['pdf', 'jpg', 'png', 'doc', 'xls'],
            size: '200mb',  
        }, async (part) => {
            filenameWithType = part.filename.split("/").pop()
            typeName = filenameWithType.split('.').pop()

            body.image = `/person/img-2022.${typeName}`
            await Drive.disk(Env.get('DRIVE_DISK')).put(body.image, part.stream, {
                ACL: 'public-read',
            })
        })

        await request.multipart.process()
      

        console.log(body.image)

        // await request.multipart.file('image', {}, async (file) => {
        //     body.image = `person/image-tms${(new Date()).getTime()}-${body.code}.${file.extname}`
        //     await Drive.disk(Env.get('DRIVE_DISK') || 'space').put(body.image, file.stream, {
        //         ACL: 'public-read',
        //     })
        // })
    }
}
