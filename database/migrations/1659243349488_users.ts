import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'users'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.string('fullname', 255).nullable()
      table.string('tel', 255).nullable()
      table.string('user_code').notNullable().unique()
      table.string('username',25).notNullable().unique()
    })
  }

  public async down () {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropColumn('fullname')
      table.dropColumn('tel')
      table.dropColumn('user_code')
      table.dropColumn('username')
    })
  }
}
