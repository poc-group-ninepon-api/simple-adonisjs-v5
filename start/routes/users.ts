import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
    Route.get('/:id', 'UsersController.show').middleware('throttle:global')
    Route.get('/', 'UsersController.index')
    Route.post('/', 'UsersController.store')
    Route.put('/', 'UsersController.update')
    Route.delete('/', 'UsersController.destroy')
  }).prefix('/v1/users').middleware('auth')
  