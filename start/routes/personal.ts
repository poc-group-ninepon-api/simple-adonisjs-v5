import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
    Route.get('/', 'PersonalsController.index')
    Route.post('/image', 'PersonalsController.uploadImagePersonToS3')
  }).prefix('/v1/person').middleware('auth')
  