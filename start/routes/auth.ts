import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
  Route.post('/login', 'UsersController.login')
  Route.post('/register-admin', 'UsersController.registerAdmin')
  Route.post('/logout', 'UsersController.logout').middleware('auth')
  Route.post('/refresh', 'UsersController.refreshToken').middleware('auth')
}).prefix('/v1/auth')
  