/*
|--------------------------------------------------------------------------
| Define HTTP rate limiters
|--------------------------------------------------------------------------
|
| The "Limiter.define" method callback receives an instance of the HTTP
| context you can use to customize the allowed requests and duration
| based upon the user of the request.
|
*/

import { Limiter } from '@adonisjs/limiter/build/services'
import User from 'App/Models/User'

export const { httpLimiters } = Limiter.define('global', ({ auth }) => {

  if (auth.user) {
    return Limiter
      .allowRequests(5000)
      .every('1 min')
      // .usingKey(user.id)
  }


  return Limiter
    .allowRequests(5)
    .every('1 min')
    .store('redis')
    .limitExceeded((error) => {
      error.message = 'Rate limit exceeded'
      error.status = 429

      // A key-value pair of headers to set on the response
      console.log(error.headers)
    })

})
